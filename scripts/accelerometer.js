// *******************************************************************
//   This class is part of the  Sizmek Accelerometer HTML5 Ad Feature 
//   ALL RIGHTS RESERVED TO © 2016 Sizmek, Inc.
// *******************************************************************
// *******************************************************************

var Accelerometer = function (_c, _pname) {
	// Save Reference to Script Name 
	this.scriptName		= "Accelerometer";
	// Script Version
	var scriptVersion	= "2.0.1";
	// Last Modified
	var lastModified	= "2016-10-19";
	// Last Uploaded
	var lastUploaded	= "2016-10-19";
	// Block Id
	this.blockId			= 6164;
	// Establish Reference to coordinate offset
	this.offset = _c || [0,45,0];

	console.log(_pname);
	// Establish if targeting panel
	this.pName = _pname;
	// Establish Boolean to track if supported.
    this.supported = 0;
    // Establish Boolean for tracking
    this.trackOnce = true;
    // Establish Reference to this
    var self = this;
    // Call Init Method once AdKit is loaded    
    adkit.onReady( function() { self.init(); } );
};

Accelerometer.prototype = {
	// Function That Creates Element Var
    d: function (id) { return document.getElementById(id); },
    
	// Initialize Scratch and Reveal Block
    init: function (){ 
    	//self.alert("cob");
    	// create reference to this
    	self = this;    	
		// Listen for EB Messages

		// Send Message
		EB._sendMessage('getAction', { offset:this.offset, pName:this.pName });		

		window.addEventListener("message", function (event) {
			// Establish Object
			var obj;
			// Evaluate JSON first before proceeding. When the jsonstring can't be evaluated, do not proceed.
        	try { obj = eval(JSON.parse(event.data)); } catch(e){ return; }
		    // Handle Messages
			self.handleData(obj);
		}); 
			
    },
    
    // DO NOT REMOVE THE FOLLOWING 
    // Track Ad Features - accepts interaction name and impression type
    trackAdFeatures: function(_interaction, _noun) { 
    	// Establish var to track local mode
    	var isServed;
    	// Verify ad is not local
    	try { isServed = EB._isServingMode(); } catch (err) {}
    	
		if (isServed && this.trackOnce){ 
			try {
				// Update Boolean
				this.trackOnce = false;	
				// Grab Ad ID
				var adId = EB._adConfig.adId;
				// Grab Session ID
				var sId = EB._adConfig.sID; 
				// Build Tracking Pixel
				var trackingPixel = "https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=19&mc=imp&pli=16479316&PluID=0&ord=%time%&rtu=-1&pcp=$$sID=" + sId + "|adID=" + adId + "|interactionName=" + _interaction + "|noun=" + _noun + "$$"; 
				// Fire Tracking Pixel by creating a new image.
				new Image().src = trackingPixel; 
				
			}  catch (err) {}
		}
    }, 
	
	// Handle Events
	addEventListener: function(_e, _m, _b) {
		// Listen for EB Messages
		window.addEventListener(_e, _m, _b); 
	},  
    
    // Handle Messages from Listener
	handleData: function(_obj) {
		// Establish var for event
		var e = document.createEvent("Event");
		
		// Listen for Custom Script for Connection Message
		if (_obj.type === 'notSupported'){
			// Init Event
			e.initEvent("orientationDataSupported", true, false);
			// Pass Success Message
			e.detail =  { 'success': false };
			// Dispatch Event
			window.dispatchEvent(e);
		}
		// Listen for Custom Script for Connection Message
		if (_obj.type === 'orientationData') {
			// Handle Feature Tracking
			this.trackAdFeatures('impression', 'AF_' + this.scriptName + '_' + this.blockId);
	    	// Update Boolean
	    	this.supported ++;
			
			// Check if handler is run more than once, if so, device orientation is supported
			if (this.supported > 50 ) {
				// Init Event
				e.initEvent("orientationData", true, false);
				// Pass Success Message
				e.detail = { 'a':_obj.data.a, 'b':_obj.data.b, 'g':_obj.data.c };
			} else { 
				// Init Event
				e.initEvent("orientationDataSupported", true, false);
				// Pass Success Message
				e.detail =  { 'success': false };
			}
			
			// Dispatch Event
			window.dispatchEvent(e);
	    } 
	}      
};