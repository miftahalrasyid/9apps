/*   Author:    Custom Format Group  */

window.ebO && ebO.extensionHooks && ebO.extensionHooks.push(function (adConfig)
{
	"use strict";
	//=================================================
	// Variables
	//-------------------------------------------------
	var scriptName = "PL_HTML5ScrollReveal";
	var scriptVersion = "1.2.1";
	var lastModified = "2016-10-17";

	var templateVersion = "2.0.15";		//template version on which this custom script is based
	var isDebug = false;				//For enabling/disabling the _self.log function (Set this to false on release)

	var adId = adConfig.adId;
	var rnd = adConfig.rnd;
	var uid = adConfig.uid;
	var _self;           //our custom object with our functions and properties (not the same as ad)
	var ad;             //the ad object that we no longer extend
	var os;
	var browser;
	// Set Default 
	var offset = [0, 45, 0];

	EBG.customFormats = EBG.customFormats || {};
	EBG.customFormats[uid] = EBG.customFormats[uid] || {};
	
	try {
		if(parent.EBG) {
			parent.EBG.customFormats = parent.EBG.customFormats || {};
			parent.EBG.customFormats[uid] = EBG.customFormats[uid];
		}
	}
	catch(e){}
	//-------------------------------------------------
	//=================================================
	// Constructor
	//-------------------------------------------------
	/**
	 * Creates the CustomFormat object
	 *
	 * @constructor
	 * @this {CustomFormat}
	 */
	function CustomFormat()
	{
		_self = this;    //use _self instead of this for guaranteed reference to this object and not window (on event handlers)	
		
		EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.SHOW_AD, _self.handlers.handleBeforeShowAd, EBG.Events.EventTiming.BEFORE,{myAd:uid});
	}
	//-------------------------------------------------
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	//=================================================
	// Public methods/functions/properties
	//-------------------------------------------------
	CustomFormat.prototype =
	{	
		isDebug: isDebug,   //this is only here so it can be overridden by a custom script plugin
		//if you want to override an event handler, overwrite it's entry in the handlers object to point to your function
		 
		//=================================================
		// Event Handlers
		//-------------------------------------------------
		handlers:
		{
			// In the event of having more than 1 subscription for a specific event/timing pair, just number each one (including the first, e.g.: SHOW_AD_BEFORE1)
			handleBeforeShowAd: 			function(){return _self._handleBeforeShowAd.apply(this,arguments);},
			handleAfterShowAd: 				function(){return _self._handleAfterShowAd.apply(this,arguments);},
			handleBeforeExpand:				function(){return _self._handleBeforeExpand.apply(this,arguments);},
			handleAfterExpand:				function(){return _self._handleAfterExpand.apply(this,arguments);},
			handleBeforeCollapse:			function(){return _self._handleBeforeCollapse.apply(this,arguments);},
			handleAfterCollapse:			function(){return _self._handleAfterCollapse.apply(this,arguments);},
			handleBeforeAddCreatives:		function(){return _self._handleBeforeAddCreatives.apply(this,arguments);},
			handleAfterAddCreatives:		function(){return _self._handleAfterAddCreatives.apply(this,arguments);},
			handleOntimePageResize: 		function(){return _self._handleOntimePageResize.apply(this,arguments);},
			handleOntimePageScroll: 		function(){return _self._handleOntimePageScroll.apply(this,arguments);},
			handleOntimeOrientation: 		function(){return _self._handleOntimeOrientation.apply(this,arguments);},
			handleCreativeContainerReady:	function(){return _self._handleCreativeContainerReady.apply(this, arguments);}
		},
		
		_handleBeforeShowAd: function(event)
		{
			_self.log("handleBeforeShowAd: isDefaultImage="+event.eventData.isDefaultImage+", dlm="+EBG.API.Ad.getAdData(uid,"dlm")+", uponShow="+EBG.API.Ad.getAdData(uid,"uponShow")); //add ,event if you want to see all properties traced
			if (event.eventData.isDefaultImage) return;     // don't do anything else if we're just serving a default image
			ad = event.dispatcher;
 			os = EBG.API.os;
			browser = EBG.API.browser;
			
			// enhanced native android browser detection
			var applewebkitString = EBG.API.os.ua.match(/applewebkit\/([\d.]+)/i);
			var applewebkitVersion = applewebkitString === null ? null : parseInt(applewebkitString.toString().split("/")[1]);
			var chromeString = EBG.API.os.ua.match(/chrome\/([\d.]+)/i);
			var chromeVersion = chromeString === null ? null : parseInt(chromeString.toString().split("/")[1]);
			
			// native android browser detection enhancement
			browser.android = os.android && (EBG.API.os.ua.match(/version/i) !== null || EBG.API.os.ua.match(/samsungbrowser/i) !== null || (applewebkitString !== null && applewebkitVersion < 537) || (chromeString !== null && chromeVersion < 45));			

			// switch off the panel VisibilityMgr
			//adConfig.panels[EBG.API.Ad.getAdData(uid,"defaultPanelName").toLowerCase()].visibilityIntEnabled = false;

			_self.isPolite = _self.wasPolite = EBG.API.Ad.getAdData(uid,"dlm") == 1;
			_self.displayWin = EBG.API.Adaptor.getDisplayWin();
			_self.displayWin.gEbPIT = _self.displayWin.gEbPIT || {};
			_self.displayWinDoc = _self.displayWin.document;
			_self.displayWinDocBody = _self.displayWinDoc.getElementsByTagName("body")[0];
			_self.subs = _self.displayWin.gEbPIT.subscriptions = _self.displayWin.gEbPIT.subscriptions || {};

			_self.isClassicInline = (os.ios && os.ver <= 7) || (os.android && os.verN < 4.3) || os.windowsphone || browser.android;
			_self.isClassicMode = _self.isClassicInline || (!os.mobile && (browser.ie || browser.edge || browser.chrome || browser.safari));
			_self.panelDocumentLoaded = false;
			_self.scrollUpward = false;
			_self.previousTop = null;
			_self.touchStartPoint = null;
			_self.touchEndPoint = null;
			_self.userIsTouching = false;
			_self.executeSnapTimer = null;
			_self.snapInAction = false;
			_self.snapedIn = false;
			_self.snapedOut = false;
			_self.panelIsLocked = false;
			_self.currentScrollTop = null;
			_self.adSizeCheckTimer = null;
			_self.videoMonitorTimer = null;
			_self.isInFullScreenPlayer = false;
			_self.isInlineScrollReveal = _self.isClassicInline;
			_self.topMargin = 0;
			_self.bottomMargin = 0;
			_self.panelScrolledIn = false;
			_self.resizeTimer = null;
			_self.maxWidthInPortraitMode = Math.min(screen.width, screen.height);
			_self.maxWidthInLandscapeMode = Math.max(screen.width, screen.height);
			_self.androidSnapThinLineFix = os.android ? 1 : 0;
			_self.previousViewPortHeight = _self.getViewPortHeight();
			_self.Const = {
				SNAP_TYPES: {
					DISABLED: 0,
					UPWARD: 1,
					DOWNWARD: 2,
					BOTH_UP_AND_DOWN: 3
				}
			};

			_self.defaultCustomFormatVars =
			{
				mdPanelWidth: null,
				mdAutoSnapType: 3, // 0: disabled, 1: upward, 2: downward, 3: both upward and downward
				mdCollapseAfterScrollAway: false,
				mdPercentageForSnapIn: 50,
				mdPercentageForSnapOut: 75,
				mdScrollAmountPerTimeUnit: 40,
				mdTopBoundingSelector: null,
				mdBottomBoundingSelector: null,
				mdDisplayTopBar: true,
				mdDisplayBottomBar: true,
				mdTopBarHeight: 20,
				mdTopBarColor: "black",
				mdTopBarLabelStyle: "color:white;font-size:10px;font-family:arial;text-align:center;",
				mdTopBarLabel: "ADVERTISEMENT",
				mdTopBarOffsetX: 0,
				mdTopBarOffsetY: 0,
				mdBottomBarHeight: 20,
				mdBottomBarColor: "black",
				mdBottomBarLabelStyle: "color:white;font-size:10px;font-family:arial;text-align:center;",
				mdBottomBarLabel: "SCROLL TO CONTINUE WITH CONTENT",
				mdBottomBarOffsetX: 0,
				mdBottomBarOffsetY: 0,
				mdTopBottomBarInCreative: "0000",	// desktop, ios, android, windows phone
				mdInlineScrollReveal: "0000",		// desktop, ios, android, windows phone
				mdDisableInlineSnap: false,
				mdEyeDivZIndex: 10000,				
				mdAdBuilder: !!EBG.API.Ad.getAdData(uid,"adBuilder") || ebO.adConfig.assets.hasOwnProperty("adDoc.json"),
				_mdDisplayAltTopBar: false,			// PRIVATE
				_mdDisplayAltBottomBar: false		// PRIVATE
			};
			
			_self.initCustomVars();
			
			// force Android Firefox to render topand bottom bars in creative
			if (os.android && browser.firefox) {
				_self.setDefault("mdTopBottomBarInCreative", "0010", true);
			}
						
			if (_self.scrollAmountPerTimeUnit <= 0)
			{
				_self.scrollAmountPerTimeUnit = _self.defaultCustomFormatVars.mdScrollAmountPerTimeUnit;
				_self.setDefault("mdScrollAmountPerTimeUnit", _self.scrollAmountPerTimeUnit, true);
			}
			
			if (_self.topBoundingSelector !== "null")
			{
				var topMarginObject = _self.displayWinDoc.querySelector(_self.topBoundingSelector);
				if (topMarginObject)
				{
					_self.topMargin = topMarginObject.getBoundingClientRect().height;
				}
			}

			if (_self.bottomBoundingSelector !== "null")
			{
				var bottomMarginObject = _self.displayWinDoc.querySelector(_self.bottomBoundingSelector);
				if (bottomMarginObject)
				{
					_self.bottomMargin = bottomMarginObject.getBoundingClientRect().height;
				}
			}			

			_self.autoSnapType = isNaN(_self.autoSnapType) || _self.autoSnapType >= 4 || _self.autoSnapType < 0 ? 0 : Math.floor(_self.autoSnapType);			
			if (_self.autoSnapType === _self.Const.SNAP_TYPES.DISABLED)
			{
				_self.disableInlineSnap = true;
				_self.setDefault("mdDisableInlineSnap", true, true);
			}

			if (_self.inlineScrollReveal.match(/^[0|1]{4}$/) === null)
			{
				_self.inlineScrollReveal = _self.defaultCustomFormatVars.mdInlineScrollReveal;
				_self.setDefault("mdInlineScrollReveal", _self.inlineScrollReveal, true);
			}
			
			if (_self.topBottomBarInCreative.match(/^[0|1]{4}$/) === null)
			{
				_self.topBottomBarInCreative = _self.defaultCustomFormatVars.mdTopBottomBarInCreative;
				_self.setDefault("mdTopBottomBarInCreative", _self.topBottomBarInCreative, true);
			}			

			if (parseInt(_self.inlineScrollReveal) !== 0)
			{
				var inlineScrollRevealEnabled = _self.getBooleanOption(_self.inlineScrollReveal);
				if ((!os.mobile && inlineScrollRevealEnabled.desktop) ||
					(os.ios && inlineScrollRevealEnabled.ios) ||
					(os.android && inlineScrollRevealEnabled.android) ||
					(os.windowsphone && inlineScrollRevealEnabled.windowsphone))
				{
					if (!_self.isClassicInline)
					{
						_self.isClassicMode = false;
						_self.isInlineScrollReveal = true;
					}
				}
			}

			if ((_self.displayTopBar || _self.displayBottomBar) && parseInt(_self.topBottomBarInCreative) !== 0)
			{
				var barInCreativeEnabled = _self.getBooleanOption(_self.topBottomBarInCreative);
				if ((!os.mobile && barInCreativeEnabled.desktop) ||
					(os.ios && barInCreativeEnabled.ios) ||
					(os.android && barInCreativeEnabled.android) ||
					(os.windowsphone && barInCreativeEnabled.windowsphone))
				{
					EBG.API.Ad.setCustomVar(uid, "_mdDisplayAltTopBar", true, true);
					EBG.API.Ad.setCustomVar(uid, "_mdDisplayAltBottomBar", true, true);
					_self.displayTopBar = false;
					_self.displayBottomBar = false;
				}
			}
			
			EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.SHOW_AD,					_self.handlers.handleAfterShowAd,			EBG.Events.EventTiming.AFTER,	{myAd:uid});
			EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.ADD_CREATIVES,				_self.handlers.handleBeforeAddCreatives,		EBG.Events.EventTiming.BEFORE,	{myAd:uid});
			EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.ADD_CREATIVES,				_self.handlers.handleAfterAddCreatives,		EBG.Events.EventTiming.AFTER,	{myAd:uid});
			EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.EXPAND,						_self.handlers.handleBeforeExpand,			EBG.Events.EventTiming.BEFORE,	{myAd:uid});
			EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.EXPAND,						_self.handlers.handleAfterExpand,			EBG.Events.EventTiming.AFTER,	{myAd:uid});
			EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.COLLAPSE,					_self.handlers.handleBeforeCollapse,			EBG.Events.EventTiming.BEFORE,	{myAd:uid});
			EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.COLLAPSE,					_self.handlers.handleAfterCollapse,			EBG.Events.EventTiming.AFTER,	{myAd:uid});
			EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.CREATIVE_CONTAINER_READY, 	_self.handlers.handleCreativeContainerReady,	EBG.Events.EventTiming.ONTIME,	{myAd:uid});
			_self.addWindowListener("message", "handleMessageReceived", _self._handleMessageReceived);
			_self.addWindowListener("keydown", "handleKeyDown", _self._handleKeyDown);			
			
			_self.ebDiv = EBG.API.Ad.getPlaceholder(uid);
			_self.iframe = EBG.API.Banner.getElements(uid).banner;
			if(!_self.isPolite)
			{
				_self._subscribeToResizeScrollOrientation("_handleBeforeShowAd:not polite");
				_self.defaultPanel = ad._panels[EBG.API.Ad.getAdData(uid,"defaultPanelName").toLowerCase()];
			}

			if (os.mobile)
			{
				_self.addWindowListener(window.navigator.msPointerEnabled ? "MSPointerDown" : "touchstart", "handleTouchStart", _self.handleTouchStart);
				_self.addWindowListener(window.navigator.msPointerEnabled ? "MSPointerUp" : (browser.android ? "touchcancel" : "touchend"), "handleTouchEnd", _self.handleTouchEnd); // android native browser does not take touchend event
				_self.addWindowListener(window.navigator.msPointerEnabled ? "MSPointerMove" : "touchmove", "handleTouchMove", _self.handleTouchMove);
			}
			else
			{
				_self.addWindowListener("mouseover", "handleMouseOver", _self.releasePanel);
				_self.addWindowListener("mouseup", "handleMouseUp", _self.releasePanel);
				_self.addWindowListener("mousedown", "handleMouseDown", _self.releasePanel);
				_self.displayWinDoc.addEventListener("mousewheel", _self.releasePanel, false);
				if (!browser.firefox)
				{
					_self.addWindowListener("mousemove", "handleMouseMove", _self.releasePanel);
					_self.displayWinDoc.addEventListener("DOMMouseScroll", _self.releasePanel, false);
				}
			}
		},
		
		_handleAfterShowAd: function(event)
		{
			_self.log("handleAfterShowAd: isDefaultImage="+event.eventData.isDefaultImage+", dlm="+EBG.API.Ad.getAdData(uid,"dlm")+", uponShow="+EBG.API.Ad.getAdData(uid,"uponShow"));  //add ,event if you want to see all properties traced
			if(!_self.isPolite)
			{
				_self.iframe = EBG.API.Banner.getElements(uid).banner;
				_self.bannerDiv = EBG.API.Banner.getElements(uid).bannerDiv;
			}	

			_self.checkMultipleAd();			
		},
		
		_handleBeforeExpand: function(event)
		{
			_self.log("_handleBeforeExpand: panelName="+event.dispatcher.panelName);  //add ,event if you want to see all properties traced
			
			_self.defaultPanel = ad._panels[EBG.API.Ad.getAdData(uid,"defaultPanelName").toLowerCase()];
			_self.panelDiv = EBG.API.Adaptor.getElementById(_self.defaultPanel.divId);
			_self.panelFrame = EBG.API.Adaptor.getElementById(_self.defaultPanel.resId);
			// whitelist the panel from viewibility report
			ad._adStackingDetector.addElementId(_self.defaultPanel.divId);

			if (_self.displayTopBar)
			{
				_self.createTextBar("topBar");
			}
			if (_self.displayBottomBar)
			{
				_self.createTextBar("bottomBar");
			}
			
			_self._handleOntimePageResize();
		},

		_handleAfterExpand: function(event)
		{
			_self.log("_handleAfterExpand: panelName="+event.dispatcher.panelName);  //add ,event if you want to see all properties traced
			_self.resizeRepositionElements();
			
			//EBG.API.Banner.setViewabilityElem(uid, event.dispatcher._iframe.id);
			if (!_self.isClassicMode) {
				EBG.API.Banner.setViewabilityElem(uid, _self.scrollRevealDiv.id);
				EBG.API.Panel.setViewabilityElem(uid, _self.defaultPanel.name, _self.scrollRevealDiv.id);
			} else {
				EBG.API.Banner.setViewabilityElem(uid, _self.ebDiv.id);
				EBG.API.Panel.setViewabilityElem(uid, _self.defaultPanel.name, _self.ebDiv.id);
			}
		},

		_handleBeforeCollapse: function(event)
		{
			_self.log("_handleBeforeCollapse: panelName="+event.dispatcher.panelName);  //add ,event if you want to see all properties traced
		},
		
		_handleAfterCollapse: function(event)
		{
			_self.log("_handleAfterCollapse: panelName="+event.dispatcher.panelName);  //add ,event if you want to see all properties traced
		},		

		_handleBeforeAddCreatives: function(event)
		{
			_self.log("_handleBeforeAddCreatives:"+event.eventData.creativeType+":panelName="+(event.dispatcher.panelName || "banner"));  //add ,event if you want to see all properties traced

			//check in event data for whether this is a banner or panel, and if a panel, which panel is it
			//you can then modify what you need to before the banner/panel are created. You can modify
			//the expand/collapse parameters here too. The ADD_CREATIVES is dispatched after the panel's
			//CC object is created with all the default parameters, but just before the HTML tags are
			//written to the page, so in addition to modify the expand/collapse params, you can also
			//modify the HTML tags that are about to be written.

			//if(event.eventData.creativeType == EBG.Events.EventNames.ADD_BANNER_PRELOAD_IMAGE_CREATIVE)	//adding the preload img
			//{
			//}

			if(event.eventData.creativeType == EBG.Events.EventNames.ADD_HTML5_MAIN_CREATIVE)	//adding the HTML5 banner
			{
				if(_self.isPolite)
				{
					_self._subscribeToResizeScrollOrientation("_handleBeforeAddCreatives:banner:was polite");
					_self.isPolite = false;	//not polite anymore (note: we still have _self.wasPolite if we want to know if we 'were')
				}
			}
			else if(event.eventData.creativeType == EBG.Events.EventNames.ADD_HTML5_PANEL_CREATIVE)
			{
				_self.eyeDiv = EBG.API.Adaptor.getEyeDiv();
				EBG.API.Adaptor.setStyle(_self.eyeDiv, {
					zIndex: _self.eyeDivZIndex
				});				
				if (!_self.isClassicMode)
				{
					_self.scrollRevealDiv = _self.displayWinDoc.createElement("div");
					_self.scrollRevealDiv.id = "ebScrollRevealDiv_" + uid;
					EBG.API.Adaptor.setStyle(_self.scrollRevealDiv, {
						position: "absolute",
						top: EBG.px(EBG.API.Adaptor.getPositioningById(_self.ebDiv.id).Y),
						left: EBG.px(_self.getPanelLeft()),
						width: EBG.px(_self.getPanelWidth()),
						height: EBG.px(_self.getPanelHeight()),
						overflow: "hidden",
						padding: "0px",
						margin: "0px",
						border: "0px",
						zIndex: 1
					});
					_self.eyeDiv.appendChild(_self.scrollRevealDiv);

					_self.scrollRevealInnerDiv = _self.displayWinDoc.createElement("div");
					EBG.API.Adaptor.setStyle(_self.scrollRevealInnerDiv, {
						position: "absolute",
						top: "0px",
						left: "0px",
						width: "100%",
						height: "100%",
						padding: "0px",
						margin: "0px",
						border: "0px"
					});			
										
					if (!os.mobile)
					{
						EBG.API.Adaptor.setStyle(_self.scrollRevealInnerDiv, {
							clip: "rect(auto auto auto auto)"
						});
					}
					
					_self.scrollRevealDiv.appendChild(_self.scrollRevealInnerDiv);	

					_self.scrollRevealContainer = _self.displayWinDoc.createElement("div");
					_self.scrollRevealContainer.id = "ebScrollRevealContainer_" + uid;
					EBG.API.Adaptor.setStyle(_self.scrollRevealContainer, {
						position: _self.isInlineScrollReveal ? "absolute" : "fixed",
						top: "0px",
						left: _self.isInlineScrollReveal ? 0 : EBG.px(_self.getPanelLeft()),
						width: EBG.px(_self.getPanelWidth()),
						height: EBG.px(_self.getPanelHeight()),
						padding: "0px",
						margin: "0px",
						border: "0px"
					});
					_self.scrollRevealInnerDiv.appendChild(_self.scrollRevealContainer);				
					
					event.eventData.assetObj.parentNodeId = _self.scrollRevealContainer.id;
				}
				//event.eventData.panelName is being added
				_self.defaultPanel = ad._panels[EBG.API.Ad.getAdData(uid,"defaultPanelName").toLowerCase()]; //you may want to know this before now, but default panel isn't "set" until first panel gets added
			}
		},
		
		_handleAfterAddCreatives: function(event)
		{
			_self.log("_handleAfterAddCreatives:"+event.eventData.creativeType+":panelName="+(event.dispatcher.panelName || "banner"));  //add ,event if you want to see all properties traced			
		},

		_subscribeToResizeScrollOrientation: function(trig)
		{
			if(!_self.subscribedRSO)	//time to subscribe to Resize/Scroll/Orientation events, as long as we didn't already do that
			{
				//comment out those you don't need, and add any mouse-based subscriptions here
				EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.PAGE_RESIZE,		_self.handlers.handleOntimePageResize,			EBG.Events.EventTiming.ONTIME,	{myAd:uid});
				EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.PAGE_SCROLL,		_self.handlers.handleOntimePageScroll,			EBG.Events.EventTiming.ONTIME,	{myAd:uid});
				EBG.API.EventManager.subscribeToEvent(EBG.Events.EventNames.SCREEN_ORIENTATION,	_self.handlers.handleOntimeOrientation,			EBG.Events.EventTiming.ONTIME);
				_self.subscribedRSO = true;
				_self.log("_subscribeToResizeScrollOrientation:triggered by "+trig);
			}
		},

		_handleKeyDown: function()
		{
			if (_self.panelIsLocked)
			{
				_self.releasePanel();
			}
		},			
		
		_handleMessageReceived: function(event, params)
		{
			try
			{
				var msg = event ? JSON.parse(event.data) : params;				
				//only messages with matching uid are handled.
				//Don't change this code to match what your assets send, change your
				//assets to match this (you NEED to send uid for multiple ads on a page).
				if(msg.type && msg.data && msg.data.uid && msg.data.uid == uid && _self.messageHandlers.hasOwnProperty(msg.type))
				{
					_self.log("_handleMessageReceived:"+msg.type,msg);
					_self.messageHandlers[msg.type](msg);
				}
			}
			catch (e)
			{
				_self.log("_handleMessageReceived:catch",e);
			}
		},
				
		messageHandlers:
		{
			addCustomScriptEventListener: function(msg)
			{
				_self.subs[msg.data.listenerId] = msg.data;
			},
			
			getDeviceOrientation: function(msg)	{
				try {
					// Remove Listener from window if it exists
					window.removeEventListener('deviceorientation', accelListener, false);
				} catch (e) {
					self.log("_handleMessageReceived:catch",e);
				}
				
				// Grab Message Data from Creative
				self.subs[msg.data.listenerId] = msg.data;
				// Update Offset from Creative Config
				offset = msg.data.offset;
				// Establish Reference for message string
				var msg;
				// Establish Protocol
	        	var proto = '';
	        	// Check for secure
	        	if (ebO.adConfig.protocol == 'https://') { proto = 'secure-'; }

	        	//Check for preview page

				
				// Determine Panel Name from Creative
				var pName = msg.data.pName || '';
				// Establish Reference to Panel 
				var panel;
				// If using Panel, save Panel Object
				if (pName !== '') { panel = EBG.API.Panel.getElements(uid, pName).panel; }
				
				// Handle Accelerometer Data
				function accelListener(e) {

					// Make sure we're passing actual data
					if (e.accelerationIncludingGravity.x !== 0 && e.accelerationIncludingGravity.y !== 0 && e.accelerationIncludingGravity.z !== 0 ){
						// alpha is the compass direction the device is facing in degrees
						var a = e.accelerationIncludingGravity.x - offset[0];
						// beta is the front-to-back tilt in degrees, where front is positive
						var b = e.accelerationIncludingGravity.y - offset[1];   
						// gamma is the left-to-right tilt in degrees, where right is positive
						var g = e.accelerationIncludingGravity.z - offset[2];
						
						var data = {a:a, b:b, c:g};
						// Create Message String
						msg = JSON.stringify({type: "orientationData", data: data });
						
				        // Send Message containing Positioning Data to Banner or Panel
						try {
							if (pName !== '') {
								panel.contentWindow.postMessage(msg, ebO.adConfig.protocol + proto + "ds.serving-sys.com");
							} else {
								self.iframe.contentWindow.postMessage(msg, ebO.adConfig.protocol + proto + "ds.serving-sys.com");
							}
						} catch (e) {
							self.log("_handleMessageReceived:catch",e);
						}
					}
				}
				
				// Check for Accelerometer
				if (window.DeviceOrientationEvent) {
					// Listen for the deviceorientation event and handle the raw data
					window.addEventListener('devicemotion', accelListener, false);
				} else {
					alert("sozm8");
					// Create Message String
					msg = JSON.stringify({type: "notSupported" });
					// Send Message containing Positioning Data to Banner or Panel
					if (pName !== '') {
						panel.contentWindow.postMessage(msg, ebO.adConfig.protocol + proto + "ds.serving-sys.com");
					} else {
						self.iframe.contentWindow.postMessage(msg, ebO.adConfig.protocol + proto + "ds.serving-sys.com");  
					}
				}






















			},

			dispatchCustomScriptEvent: function(msg)
			{
				for (var i in _self.subs)
				{
					if (!_self.subs[i])
						continue;
					var isEventMatch = _self.subs[i].eventName === msg.data.eventName;
					var isCurrentAd = msg.data.uid === _self.subs[i].uid;
					var isOutOfAdScope = !isCurrentAd && !msg.data.interAd;
					if (!isEventMatch || isOutOfAdScope)
						continue;
					if (_self.subs[i].callback)
					{
						try
						{
							_self.subs[i].callback(msg.data);
						}
						catch(e)
						{
							delete _self.subs[i];		//delete 'lost' listener
						}
					}
					else
					{
						var listenerIds = [];
						listenerIds[listenerIds.length] = _self.subs[i].listenerId;
						msg.data.listenerIds = listenerIds;
						try{_self.CCs[_self.subs[i].creativeIFrameId]._sendMessage("eventCallback", msg.data);}catch (error) {}
					}
				}
			},

			removeCustomScriptEventListener: function(msg)
			{
				delete _self.subs[msg.data.listenerId];
				if(msg.data.creativeIFrameId) delete _self.CCs[msg.data.creativeIFrameId];
			},

			ebclickthrough: function(msg)
			{
				// this is for ios 7 and earlier to resize the panel properly after click through because the browser toolbars changed when click through launch
				if (os.ios && os.ver < 8)
				{
					_self.currentScrollTop = _self.getScrollTop();
					setTimeout(function() {
						_self.displayWin.scrollTo(0, _self.currentScrollTop - (_self.getViewPortHeight() > 450 ? 11 : 0));
						_self._handleOntimePageResize();
					}, 1500);
				}
			},

			ebStartVideoTimer: function(msg)
			{
				if ((os.ios && !os.ipad && os.verN < 9.3) || (os.android && os.ver === 2))
				{
					_self.isInFullScreenPlayer = true;
				}
				else
				{
					if (_self.videoMonitorTimer === null)
					{
						_self.videoMonitorTimer = setInterval(function() {
							if (_self.ebDiv.getBoundingClientRect().top > _self.getViewPortHeight() || _self.ebDiv.getBoundingClientRect().bottom < 0)
							{
								_self.panelScrollAway();
							}
						}, 2000);
					}
				}
			},

			ebendvideotimer: function(msg)
			{
				if ((os.ios && !os.ipad) || (os.android && os.ver === 2))
				{
					_self.isInFullScreenPlayer = false;
				}
				if (_self.videoMonitorTimer)
				{
					clearInterval(_self.videoMonitorTimer);
				}
				_self.videoMonitorTimer = null;
			},

			ebVideoInteraction: function(msg)
			{
				if (msg.data.intName === "ebFSStart")
				{
					_self.isInFullScreenPlayer = true;
				}
				if (msg.data.intName === "ebFSEnd")
				{
					_self.isInFullScreenPlayer = false;
				}				
			},

			panelIsLoaded: function(msg)
			{
				if (msg.data.uid === _self.defaultPanel.CC._adConfig.uid)
				{
					setTimeout(function() {
						_self.panelDocumentLoaded = true;
						if(!_self.isClassicMode)
						{
							EBG.API.Adaptor.setStyle(_self.scrollRevealDiv, {
								display: "block"
							});
							_self._handleOntimePageResize();
						}
					}, 500);
				}
			},

			setCreativeVersion: function(msg)
			{
				if (msg.data.creativeVersion)
				{
					_self.handleSetCreativeVersion(msg.data);
				}
			},

			creativeTouchStart: function(msg)
			{
				_self.creativeTouchStart(msg.data);
			},
			
			creativeTouchEnd: function(msg)
			{
				_self.creativeTouchEnd(msg.data);
			},

			creativeEnterFullScreen: function()
			{
				_self.creativeEnterFullScreen();
			},			
			
			creativeExitFullScreen: function()
			{
				_self.creativeExitFullScreen();
			},

			creativeWheelScroll: function()
			{
				_self.releasePanel();
			}
		},
		
		_handleOntimePageResize: function()
		{
			// prevent page zoom
			if (os.mobile && !_self.isClassicInline && !_self.isClassicMode)
			{
				if (_self.getViewPortWidth() < _self.getViewPortHeight())
				{
					EBG.API.Adaptor.setStyle(_self.scrollRevealDiv, {
						width: EBG.px(Math.min(_self.maxWidthInPortraitMode, _self.getPanelWidth()))
					});
				}
				else
				{
					EBG.API.Adaptor.setStyle(_self.scrollRevealDiv, {
						width: EBG.px(Math.min(_self.maxWidthInLandscapeMode, _self.getPanelWidth()))
					});
				}
			}
			
			if (os.ios && os.ver === 7)
			{				
				if (_self.panelDiv.getBoundingClientRect().left < 0)
				{
					_self.displayWin.scrollTo(0, _self.getScrollTop());
				}
				if (_self.getViewPortWidth() < _self.getViewPortHeight())
				{
					EBG.API.Adaptor.setStyle(_self.displayWinDocBody, {
						width: EBG.px(_self.maxWidthInPortraitMode)
					});
				}
				else
				{
					EBG.API.Adaptor.setStyle(_self.displayWinDocBody, {
						width: EBG.px(_self.maxWidthInLandscapeMode)
					});
				}
			}			

			if (!_self.shouldBeResized())
			{
				return;
			}
			
			if (!os.mobile)
			{
				_self.isInFullScreenPlayer = _self.getViewPortHeight() === screen.height;
			}
			
			if (os.android && os.ver === 4)
			{
				if (_self.android4CheckSizeTimer)
				{
					clearTimeout(_self.android4CheckSizeTimer)
				}
				_self.android4CheckSizeTimer = setTimeout(function() {
					if (_self.getViewPortWidth() > _self.getViewPortHeight())
					{
						if (_self.panelDiv.getBoundingClientRect().width < _self.getViewPortWidth())
						{
							_self.resizeRepositionElements();
						}
					}
				}, 2000);
			}
			
			setTimeout(_self.resizeRepositionElements, os.ios ? 250: 1);
		},
		
		_handleOntimePageScroll: function(e)
		{
			_self.setScrollRevealClip();
			if (_self.isClassicMode)
			{
				_self._handleOntimePageResize();
			}

			var eventType = typeof e !== "undefined" ? "scroll" : "touchend";

			if (_self.executeSnapTimer)
			{
				clearTimeout(_self.executeSnapTimer);
			}
			
			if (!_self.panelIsLocked)
			{
				if (_self.touchStartPoint !== null && _self.touchEndPoint !== null)
				{
					if (eventType === "scroll")
					{
						_self.scrollUpward = _self.touchStartPoint > _self.touchEndPoint;
					}
					if (_self.previousTop !== _self.getScrollTop())
					{
						_self.scrollUpward = _self.previousTop < _self.getScrollTop();
					}					
				}
				else
				{
					_self.scrollUpward = _self.previousTop < _self.getScrollTop();
				}
				
				if (eventType === "scroll")
				{
					_self.previousTop = _self.getScrollTop();
				}

				if ((_self.scrollUpward && _self.ebDiv.getBoundingClientRect().bottom < 0) || (!_self.scrollUpward && _self.ebDiv.getBoundingClientRect().top > _self.getViewPortHeight()))
				{
					if (_self.panelScrolledIn)
					{
						_self.panelScrollAway();
					}
				}
				else if ((_self.scrollUpward && _self.ebDiv.getBoundingClientRect().top < _self.getViewPortHeight()) || (!_self.scrollUpward && _self.ebDiv.getBoundingClientRect().bottom > 0))
				{
					if (!_self.panelScrolledIn)
					{
						_self.panelScrollIn();
					}
				}
				
				if ((_self.ebDiv.getBoundingClientRect().top < _self.getViewPortHeight() && _self.ebDiv.getBoundingClientRect().top > 0) || (_self.ebDiv.getBoundingClientRect().bottom > 0 && _self.ebDiv.getBoundingClientRect().bottom < _self.getViewPortHeight()))
				{
					EBG.API.Adaptor.setStyle(_self.panelFrame, {
						display: "block"
					});
				}

				if (_self.autoSnapType !== _self.Const.SNAP_TYPES.DISABLED && !_self.snapInAction)
				{
					_self.executeSnapTimer = setTimeout(function() {
						var doSnap = ((os.mobile && eventType === "touchend") || 										// when touch end on ios browsers
							(os.ios && eventType === "scroll" && !_self.userIsTouching && _self.getScrollTop() > 0) || 	// when scroll on ios 7 and earlier browsers
							(!os.ios && eventType === "scroll"))														// when scroll on other browsers					
							&& (!_self.isInlineScrollReveal || (_self.isInlineScrollReveal && !_self.disableInlineSnap))
						if (_self.panelDocumentLoaded && doSnap && !_self.isInFullScreenPlayer)
						{
							_self.snapAd();
						}
					}, browser.android ? 500 : 100);
				}

				if (_self.snapedIn)
				{
					_self.snapedIn = false;
				}
				if (_self.snapedOut)
				{
					_self.snapedOut = false;
				}
			}
		},

		_handleOntimeOrientation: function()
		{
			// prevent page zoom
			if (os.mobile && !_self.isClassicInline)
			{
				if ((os.android && os.ver === 4 && browser.android) || (os.ios && (_self.trim(os.verS) === "9.1" || _self.trim(os.verS) === "9.2.1")))
				{
					EBG.API.Adaptor.setStyle(_self.scrollRevealDiv, {
						display: "none",
						overflow: "hidden"
					});					
					EBG.API.Adaptor.setStyle(_self.panelFrame, {
						display: "none"
					});
					
					if (_self.iosZoomFixTimer)
					{
						clearTimeout(_self.iosZoomFixTimer);
					}
					if (os.ios)
					{
						_self.iosZoomFixTimer = setTimeout(function(){
							if (_self.getViewPortWidth() < _self.getViewPortHeight() && _self.getViewPortWidth() > _self.maxWidthInPortraitMode)
							{
								EBG.API.Adaptor.setStyle(_self.scrollRevealDiv, {
									width: EBG.px(_self.maxWidthInPortraitMode)
								});
								EBG.API.Panel.modify(uid, ad._defaultPanel.toLowerCase(), {
									width: _self.maxWidthInPortraitMode
								}, true);
								EBG.API.Adaptor.setStyle(_self.displayWinDocBody, {
									width: EBG.px(_self.maxWidthInPortraitMode)
								});
							}
						}, 1500);
					}
				}

				if (!(browser.android && (_self.trim(os.verS) === "4.1.1" || _self.trim(os.verS) === "4.1.2" || _self.trim(os.verS) === "4.2.2")))
				{
					if (_self.getViewPortWidth() < _self.getViewPortHeight())
					{
						EBG.API.Adaptor.setStyle(_self.displayWinDocBody, {
							width: EBG.px(_self.maxWidthInPortraitMode)
						});
					}
					else
					{
						EBG.API.Adaptor.setStyle(_self.displayWinDocBody, {
							width: EBG.px(Math.min(_self.maxWidthInLandscapeMode, _self.getViewPortWidth()))
						});
					}
				}

				setTimeout(_self.resizeRepositionElements, 500);
			}
			else
			{
				_self.resizeRepositionElements();
			}
			if (_self.resizeTimer)
			{
				clearTimeout(_self.resizeTimer);
			}
			if (os.mobile)
			{
				_self.resizeTimer = setTimeout(function() {
					if (_self.panelDiv.getBoundingClientRect().width !== _self.getViewPortWidth())
					{
						_self.resizeRepositionElements();
					}
				}, 500);
			}
		},

		_handleCreativeContainerReady: function(event)
        {
        	_self.log("_handleCreativeContainerReady:"+(event.dispatcher.panelName || "banner"));

        	if(!event.dispatcher.panelName)	//is this the CCR for the banner?
        	{
				_self.iframe = EBG.API.Banner.getElements(uid).banner;
				_self.bannerDiv = EBG.API.Banner.getElements(uid).bannerDiv;
			}
			var ifrmCC = event.dispatcher;
			_self.CCs = _self.CCs || {};			
			_self.CCs[ifrmCC.iframeId] = ifrmCC;
			ifrmCC._sendMessage("sendCreativeId", {creativeIFrameId:ifrmCC.iframeId, uid:uid});
        },		
		//-------------------------------------------------
		//End of Event Handlers Section
		//=================================================	

		//=================================================
		// Custom Event Method
		//-------------------------------------------------
		shouldBeResized: function()
		{
			if (!_self.scrollUpward && os.mobile && (_self.ebDiv.getBoundingClientRect().bottom < Math.floor(_self.getViewPortHeight() * 0.90) || _self.ebDiv.getBoundingClientRect().bottom > _self.getViewPortHeight()))
			{
				return false;
			}
			return true;
		},
		
		checkAdVisibility: function()
		{
			if (_self.ebDiv !== null)
			{
				// check to see if the ad is loaded into hidden container
				var ebDivParent = _self.ebDiv.parentNode;
				do
				{
					if (EBG.API.Adaptor.getStyle(ebDivParent, "display") === "none" || EBG.API.Adaptor.getStyle(ebDivParent, "visibility") === "hidden")
					{
						return true;
					}
					ebDivParent = ebDivParent.parentNode;
				}
				while (ebDivParent && ebDivParent.hasOwnProperty("tagName") && ebDivParent.tagName.toLowerCase() !== "body")
				return false;
			}
			return true;
		},
		
		checkMultipleAd: function()
		{			
			if (_self.ebDiv !== null)
			{
				// check to see if there is another scroll reveal served on the page and prevent it load if the newer one will sitting on the top of the old ones
				var scrollRevealCollection = _self.displayWinDoc.getElementsByClassName("ebScrollRevealAd");
				for (var i = 0; i< scrollRevealCollection.length; i++)
				{
					var existingAdPosition = EBG.API.Adaptor.getPositioningById(scrollRevealCollection[i].id);
					var existingAdBottom = existingAdPosition.Y + _self.getPanelHeight();
					var newAdPosition = EBG.API.Adaptor.getPositioningById(_self.ebDiv.id);
					var newAdBottom = newAdPosition.Y + _self.getPanelHeight();
					
					if ((newAdPosition.Y === existingAdPosition.Y) ||
						(newAdPosition.Y < existingAdPosition.Y && newAdBottom > existingAdPosition.Y) ||
						(newAdPosition.Y > existingAdPosition.Y && newAdPosition.Y < existingAdBottom))
					{
						// remove new ad if new ad is sitting on the top of the old ad
						_self.removeScrollReveal();
					}
				}
				_self.ebDiv.className += " ebScrollRevealAd";
			}
		},
		
		setScrollRevealClip: function()
		{
			if (os.mobile && !_self.isClassicMode && !_self.isInlineScrollReveal)
			{
				EBG.API.Adaptor.setStyle(_self.scrollRevealContainer, {
					clip: "rect(" + _self.ebDiv.getBoundingClientRect().top +"px, " + _self.getPanelWidth() + "px, " + _self.ebDiv.getBoundingClientRect().bottom +"px, 0px)"
				});
			}			
		},
		
		resizeRepositionElements: function()
		{
			var adIsHidden = _self.checkAdVisibility();
			if (adIsHidden)
			{
				if (os.ios && os.ver < 8)
				{
					ebDivParent = _self.ebDiv.parentNode;
					do
					{
						EBG.API.Adaptor.setStyle(ebDivParent, {
							height: "initial"
						});
						ebDivParent = ebDivParent.parentNode;
					}
					while (ebDivParent.tagName.toLowerCase() !== "body")
				}
			}

			EBG.API.Adaptor.setStyle(_self.ebDiv, {
				display: "block",
				height: EBG.px(adIsHidden ? adConfig.height : _self.getPanelHeight()),
				width: EBG.px(adIsHidden ? adConfig.width : Math.min(300, _self.getPanelWidth()))
			});
			
			EBG.API.Adaptor.setStyle(_self.iframe, {
				height: EBG.px(adIsHidden ? adConfig.height : _self.getPanelHeight())
			});			
			
			EBG.API.Adaptor.setStyle(_self.eyeDiv, {
				display: "block"
			});

			_self.setScrollRevealClip();			
			
			if (_self.isClassicMode)
			{
				if (_self.isClassicInline)
				{
					EBG.API.Adaptor.setStyle(_self.panelDiv, {
						position: "absolute",
						overflow: "hidden"
					});
					EBG.API.Panel.modify(uid, ad._defaultPanel.toLowerCase(), {
						left: _self.getPanelLeft(),
						top: EBG.API.Adaptor.getPositioningById(_self.ebDiv.id).Y
					}, true);
				}
				else
				{
					EBG.API.Adaptor.setStyle(_self.panelDiv, {
						position: "fixed",
						overflow: "hidden",
						clip: "rect(" + _self.getClipTop() + "px, " + _self.getPanelWidth() + "px, " + _self.getClipBottom() + "px, 0px)"
					});
				}
			}
			else
			{
				if (EBG.API.Adaptor.getPositioningById(_self.scrollRevealDiv.id).Y !== EBG.API.Adaptor.getPositioningById(_self.ebDiv.id).Y)
				{
					EBG.API.Adaptor.setStyle(_self.scrollRevealDiv, {
						top: EBG.px(EBG.API.Adaptor.getPositioningById(_self.ebDiv.id).Y)
					});
				}
				
				EBG.API.Adaptor.setStyle(_self.scrollRevealDiv ,{
					left: EBG.px(_self.getPanelLeft()),
					width: EBG.px(_self.getPanelWidth()),
					height: EBG.px(_self.getPanelHeight()),
					display: _self.panelDocumentLoaded && !adIsHidden ? "block" : "none"
				});
				
				EBG.API.Adaptor.setStyle(_self.scrollRevealContainer, {
					left: _self.isInlineScrollReveal ? 0 : EBG.px(_self.getPanelLeft())
				});				
			}

			EBG.API.Panel.modify(uid, ad._defaultPanel.toLowerCase(), {
				width: _self.getPanelWidth(),
				height: _self.getPanelHeight()
			}, true);
			
			if (!_self.isInlineScrollReveal)
			{
				EBG.API.Panel.modify(uid, ad._defaultPanel.toLowerCase(), {
					left: _self.isClassicMode ? _self.getPanelLeft() : 0,
					top: _self.topMargin,
				}, true);
			}
						
			if (_self.displayTopBar)
			{
				EBG.API.Adaptor.setStyle(_self.topBarDiv, {
					position: "absolute",
					width: EBG.px(_self.getPanelWidth()),
					top: EBG.px(_self.getTopBarTop()),
					left: EBG.px(_self.getBarLeft() + _self.topBarOffsetX)
				});
			}
			if (_self.displayBottomBar)
			{
				EBG.API.Adaptor.setStyle(_self.bottomBarDiv, {
					position: "absolute",
					width: EBG.px(_self.getPanelWidth()),
					top: EBG.px(_self.getBottomBarTop()),
					left: EBG.px(_self.getBarLeft() + _self.bottomBarOffsetX)
				});
			}
			
			if (os.mobile && !_self.isClassicMode && !_self.isInFullScreenPlayer)
			{
				EBG.API.Adaptor.setStyle(_self.panelFrame, {
					display: "block"
				});				
				if (_self.scrollRevealDiv.getBoundingClientRect().top > _self.getViewPortHeight() || _self.scrollRevealDiv.getBoundingClientRect().bottom < 0)
				{
					EBG.API.Adaptor.setStyle(_self.panelFrame, {
						display: "none"
					});
				}
			}			
		},
		
		upwardSnapIn: function()
		{
			_self.scrollByTotalAmount = Math.ceil(_self.ebDiv.getBoundingClientRect().top - _self.topMargin);
			if (!_self.isInFullScreenPlayer && _self.scrollByTotalAmount > 0)
			{
				_self.snapInAction = true;
				if (os.windowsphone)
				{
					_self.displayWin.scrollBy(0, _self.scrollByTotalAmount);
					setTimeout(_self.panelSnapIn, 50);
				}
				else
				{
					var scrollTimeFrames = Math.floor(_self.scrollByTotalAmount / _self.scrollAmountPerTimeUnit);
					for(var i = 0; i < scrollTimeFrames; i++)
					{
						setTimeout(function() {
							if (_self.ebDiv.getBoundingClientRect().top > 0)
							{
								_self.displayWin.scrollBy(0, _self.scrollAmountPerTimeUnit);
								_self.scrollByTotalAmount -= _self.scrollAmountPerTimeUnit;
							}
						}, 10 * i);
					}
					setTimeout(function() {
						if (_self.ebDiv.getBoundingClientRect().top > _self.topMargin) 
						{
							_self.displayWin.scrollBy(0, _self.ebDiv.getBoundingClientRect().top - _self.topMargin);
						}
						_self.panelSnapIn();		
					}, 10 * scrollTimeFrames);
				}
			}
		},
		
		upwardSnapOut: function()
		{
			_self.scrollByTotalAmount = Math.ceil(_self.ebDiv.getBoundingClientRect().bottom);
			if (_self.scrollByTotalAmount > 0)
			{
				_self.snapInAction = true;
				if (os.windowsphone)
				{
					_self.displayWin.scrollBy(0, _self.scrollByTotalAmount);
					setTimeout(_self.panelSnapOut, 50);
				}
				else
				{
					var scrollTimeFrames = Math.floor(_self.scrollByTotalAmount / _self.scrollAmountPerTimeUnit);
					for(var i = 0; i < scrollTimeFrames - 1; i++)
					{
						setTimeout(function() {
							if (_self.ebDiv.getBoundingClientRect().bottom > 0)
							{
								_self.displayWin.scrollBy(0, _self.scrollAmountPerTimeUnit);
								_self.scrollByTotalAmount -= _self.scrollAmountPerTimeUnit;
							}
						}, 10 * i);
					}
					
					setTimeout(function() {
						if (_self.ebDiv.getBoundingClientRect().bottom > _self.topMargin)
						{
							_self.displayWin.scrollBy(0, _self.ebDiv.getBoundingClientRect().bottom - _self.topMargin);
						}					
						_self.panelSnapOut();
					}, 10 * (scrollTimeFrames - 1));
				}
			}
		},
		
		downwardSnapIn: function()
		{
			_self.scrollByTotalAmount = Math.ceil(_self.getViewPortHeight() - (_self.ebDiv.getBoundingClientRect().bottom + _self.bottomMargin));
			if (!_self.isInFullScreenPlayer && _self.scrollByTotalAmount > 0)
			{
				_self.snapInAction = true;
				if (os.windowsphone)
				{
					_self.displayWin.scrollBy(0, 0 - _self.scrollByTotalAmount);
					setTimeout(_self.panelSnapIn, 50);
				}
				else
				{
					var scrollTimeFrames = Math.floor(_self.scrollByTotalAmount / _self.scrollAmountPerTimeUnit);
					for(var i = 0; i < scrollTimeFrames - 1; i++)
					{
						setTimeout(function() {
							if (_self.ebDiv.getBoundingClientRect().bottom < _self.getViewPortHeight() - _self.bottomMargin)
							{
								_self.displayWin.scrollBy(0, 0 - _self.scrollAmountPerTimeUnit);
								_self.scrollByTotalAmount -= _self.scrollAmountPerTimeUnit;
							}
						}, 10 * i);
					}
					setTimeout(function() {
						if (_self.ebDiv.getBoundingClientRect().bottom < _self.getViewPortHeight() - _self.bottomMargin)
						{
							_self.displayWin.scrollBy(0, 0 - (((_self.getViewPortHeight() - _self.bottomMargin) - _self.ebDiv.getBoundingClientRect().bottom) + _self.androidSnapThinLineFix));
						}					
						_self.panelSnapIn();
					}, 10 * (scrollTimeFrames - 1));
				}
			}
		},
		
		downwardSnapOut: function()
		{
			_self.scrollByTotalAmount = Math.ceil(_self.getViewPortHeight() - _self.ebDiv.getBoundingClientRect().top);
			if (_self.scrollByTotalAmount > 0)
			{
				_self.snapInAction = true;
				if (os.windowsphone)
				{
					_self.displayWin.scrollBy(0, 0 - _self.scrollByTotalAmount);
					setTimeout(_self.panelSnapOut, 50);
				}
				else
				{
					var scrollTimeFrames = Math.floor(_self.scrollByTotalAmount / _self.scrollAmountPerTimeUnit);
					for(var i = 0; i < scrollTimeFrames - 1; i++)
					{
						setTimeout(function() {
							if (_self.ebDiv.getBoundingClientRect().top < _self.getViewPortHeight() - _self.bottomMargin)
							{
								_self.displayWin.scrollBy(0, 0 - _self.scrollAmountPerTimeUnit);
								_self.scrollByTotalAmount -= _self.scrollAmountPerTimeUnit;
							}
						}, 10 * i);
					}
					
					setTimeout(function() {
						if (_self.ebDiv.getBoundingClientRect().top < _self.getViewPortHeight() - _self.bottomMargin)
						{
							_self.displayWin.scrollBy(0, 0 - ((_self.getViewPortHeight() - _self.bottomMargin) - _self.ebDiv.getBoundingClientRect().top));
						}
						_self.panelSnapOut();
					}, 10 * (scrollTimeFrames - 1));
				}
			}
		},
		
		snapAd: function()
		{
			if (_self.scrollUpward)
			{
				if (_self.autoSnapType === _self.Const.SNAP_TYPES.UPWARD || _self.autoSnapType === _self.Const.SNAP_TYPES.BOTH_UP_AND_DOWN)
				{
					if (!_self.snapedIn && _self.ebDiv.getBoundingClientRect().top < (_self.getViewPortHeight() * (100 - _self.percentageForSnapIn)) / 100)
					{
						_self.upwardSnapIn();
					}
					if (!_self.snapedOut && _self.ebDiv.getBoundingClientRect().bottom < (_self.getViewPortHeight() * (100 - _self.percentageForSnapOut)) / 100)
					{
						_self.upwardSnapOut();
					}
				}
			}
			else
			{
				if (_self.autoSnapType === _self.Const.SNAP_TYPES.DOWNWARD || _self.autoSnapType === _self.Const.SNAP_TYPES.BOTH_UP_AND_DOWN)
				{
					if (!_self.snapedIn && _self.ebDiv.getBoundingClientRect().bottom > (_self.getViewPortHeight() * _self.percentageForSnapIn) / 100)
					{
						_self.downwardSnapIn();
					}
					if (!_self.snapedOut && _self.ebDiv.getBoundingClientRect().top > (_self.getViewPortHeight() * _self.percentageForSnapOut) / 100)
					{
						_self.downwardSnapOut();
					}
				}
			}
		},

		creativeEnterFullScreen: function()
		{
			_self.isInFullScreenPlayer = true;
		},
		
		creativeExitFullScreen: function()
		{
			if (os.ios && os.ver < 8)	// fix iOS 7 the page height increased after video plays in fullscreen and rotated
			{
				_self.displayWin.scrollBy(0, 1);
			}
			_self.isInFullScreenPlayer = false;
		},
		
		removeNavigationBar: function()
		{
			setTimeout(function() {
				_self.displayWin.scrollBy(0, 1);
			}, 1000);
		},	
		
		creativeTouchStart: function(data)
		{
			_self.touchStartPoint = data.y;
			_self.handleTouchStart();
		},	
		
		creativeTouchEnd: function(data)
		{
			_self.touchEndPoint = data.y;
			_self.handleTouchEnd();
		},
		
		handleTouchStart: function(e)
		{
			if (typeof e !== "undefined")
			{
				_self.touchStartPoint = window.navigator.msPointerEnabled ? (e.pageY || event.targetTouches[0].pageY) : e.changedTouches[0].pageY;
			}			
			_self.userIsTouching = true;
		},
		
		handleTouchEnd: function(e)
		{
			if (typeof e !== "undefined")
			{
				_self.touchEndPoint = window.navigator.msPointerEnabled ? (e.pageY || event.targetTouches[0].pageY) : e.changedTouches[0].pageY;
			}
			_self.userIsTouching = false;
			_self._handleOntimePageScroll();
		},

		handleTouchMove: function(e)	
		{
		},
		
		createTextBar: function(barName)
		{		
			var barZIndex = _self.isClassicMode ? (_self.eyeDivZIndex + 1) : 2;
			var barTopPosition = _self.getTopBarTop();
			var barLeftPosition = _self.getBarLeft() + _self.topBarOffsetX;
			if (barName === "bottomBar")
			{
				barTopPosition = _self.getBottomBarTop();
				barLeftPosition = _self.getBarLeft() + _self.bottomBarOffsetX;
			}

			var barObject = {	
					tagName: EBG.Adaptors.TagNames.DIV,
					attributes:	{
						id: barName + "_" + uid,
						style: {
							position: "absolute",
							top: EBG.px(barTopPosition),
							left: EBG.px(barLeftPosition),
							width: EBG.px(_self.getPanelWidth()),
							height: EBG.px(_self[barName + "Height"]),
							lineHeight: EBG.px(_self[barName + "Height"]),
							backgroundColor: _self[barName + "Color"],
							zIndex: barZIndex
						}		
					}
				};			
			
			if (_self.isClassicMode)
			{
				_self[barName + "Div"] = EBG.API.Adaptor.inject(barObject, EBG.API.InjectionMethod.INSERT_BEFORE, _self.eyeDiv.nextSibling, uid);
			}
			else
			{
				if (barName === "topBar")
				{
					_self[barName + "Div"] = EBG.API.Adaptor.inject(barObject, EBG.API.InjectionMethod.INSERT_BEFORE, _self.scrollRevealInnerDiv, uid);
				}
				else
				{
					_self[barName + "Div"] = EBG.API.Adaptor.inject(barObject, EBG.API.InjectionMethod.APPEND, _self.scrollRevealDiv, uid);
				}
			}
			_self[barName + "Div"].style.cssText += ";" + _self[barName + "LabelStyle"];
			_self[barName + "Div"].innerHTML = _self[barName + "Label"];
			
			// whitelist the text bars from viewibility report, may not necessary when inject method is updated
			ad._adStackingDetector.addElementId(barName + "_" + uid);			
		},
		
		removeScrollReveal: function()
		{
			if (_self.scrollRevealDiv)
			{
				_self.scrollRevealDiv.parentNode.removeChild(_self.scrollRevealDiv);
			}
			if (_self.displayTopBar)
			{
				_self.topBarDiv.parentNode.removeChild(_self.topBarDiv);
			}
			if (_self.displayBottomBar)
			{
				_self.bottomBarDiv.parentNode.removeChild(_self.bottomBarDiv);
			}
			_self.ebDiv.parentNode.removeChild(_self.ebDiv);
			_self.autoSnapType = _self.Const.SNAP_TYPES.DISABLED;			
		},

		lockDownPanel: function()
		{
			if (!os.mobile)
			{
				EBG.API.Adaptor.hideScrollBars();
				_self.currentScrollTop = _self.getScrollTop();
				_self.panelIsLocked = true;
				if (_self.adBuilder)
				{
					setTimeout(_self.releasePanel, 500);
				}
			}
		},
		
		releasePanel: function()
		{
			if (!os.mobile && _self.panelIsLocked)
			{
				EBG.API.Adaptor.restoreScrollBarsState();
				_self.displayWin.scrollTo(0 , _self.currentScrollTop);
				_self._handleOntimePageResize();
				_self.panelIsLocked = false;
			}
		},
		
		collapsePanel: function()
		{
			ad._panels[ad._defaultPanel.toLowerCase()].CC.collapse(true);
			ad._CCs[_self.defaultPanel.resId]._sendMessage("adCollapsed");
			_self.removeScrollReveal();
		},
		
		panelSnapIn: function()
		{
			_self.snapedIn = true;
			_self.snapInAction = false;
			_self.resizeRepositionElements();
			if (os.mobile && !_self.scrollUpward)
			{
				_self.checkViewPortHeight();
			}
			if (os.ios)
			{
				_self.adSizeCheckTimer = setInterval(function() {
					if (_self.ebDiv.getBoundingClientRect().bottom > _self.getViewPortHeight())
					{
						_self._handleOntimePageResize();						
						clearInterval(_self.adSizeCheckTimer);
					}
				}, 50);
			}			
			_self.previousTop = _self.getScrollTop();
			_self.lockDownPanel();
			ad._CCs[_self.defaultPanel.resId]._sendMessage("adSnapIn");
		},
		
		panelSnapOut: function()
		{
			_self.snapedOut = true;
			_self.snapInAction = false;
			_self.previousTop = _self.getScrollTop();
			
			if (_self.collapseAfterScrollAway && !_self.isInFullScreenPlayer)
			{
				_self.collapsePanel();
			}
			
			setTimeout(function()
			{
				if (!_self.isInFullScreenPlayer)
				{
					_self.messageHandlers.ebendvideotimer();
					ad._CCs[_self.defaultPanel.resId]._sendMessage("adSnapOut");
				}
			}, browser.ie ? 1000: 1);
		},
		
		panelScrollIn: function()
		{
			_self.panelScrolledIn = true;
			if (os.mobile && !_self.isClassicMode)
			{
				EBG.API.Adaptor.setStyle(_self.scrollRevealDiv, {
					zIndex: 10
				});				
				EBG.API.Adaptor.setStyle(_self.panelFrame, {
					display: "block"
				});
			}
			ad._CCs[_self.defaultPanel.resId]._sendMessage("adScrollIn");
		},	
		
		panelScrollAway: function()
		{					
			if (_self.checkViewPortHeightTimer)
			{
				clearTimeout(_self.checkViewPortHeightTimer);
			}
			
			setTimeout(function(){
				_self.panelScrolledIn = false;
				if (!os.mobile)
				{
					_self.isInFullScreenPlayer = _self.getViewPortHeight() === screen.height;
				}				
				if (os.mobile && !_self.isClassicMode && !_self.isInFullScreenPlayer)
				{
					EBG.API.Adaptor.setStyle(_self.scrollRevealDiv, {
						zIndex: 1
					});				
					EBG.API.Adaptor.setStyle(_self.panelFrame, {
						display: "none"
					});
				}
				if (_self.collapseAfterScrollAway && !_self.isInFullScreenPlayer)
				{
					_self.collapsePanel();
				}	
			}, 250);
						
			setTimeout(function()
			{	
				if (!_self.isInFullScreenPlayer)
				{
					ad._CCs[_self.defaultPanel.resId]._sendMessage("adScrollAway");
					_self.messageHandlers.ebendvideotimer();
				}
			}, browser.ie ? 1000: 1);			
		},
		
		checkViewPortHeight: function()
		{
			
			if (_self.previousViewPortHeight !== _self.getViewPortHeight())
			{
				_self.resizeRepositionElements();
			}
			_self.checkViewPortHeightTimer = setTimeout(_self.checkViewPortHeight, 10);
			_self.previousViewPortHeight = _self.getViewPortHeight();
		},
		//-------------------------------------------------
		//End of Custom Event Method
		//=================================================

		//=================================================
		// Custom Vars Related Functions
		//-------------------------------------------------
		initCustomVars: function()
		{
			//_self.log("initCustomVars",_self.defaultCustomFormatVars);
			for (var cv in _self.defaultCustomFormatVars)
			{
				if (!_self.defaultCustomFormatVars.hasOwnProperty(cv)) continue;
				_self.setDefault(cv, _self.defaultCustomFormatVars[cv]); // once undefined allowed to be set by type (not "undefined"), replace this line with API setCustomVar
			}
			_self.setDefaultWithAppend("mdCustomFormatScriptVer",scriptName + "," + scriptVersion,"|");
		},

		setDefault: function(varName, defaultValue, optional_override)
		{
			//_self.log("setDefault",arguments);
			EBG.API.Ad.setCustomVar(uid, varName, defaultValue, !!optional_override);
			var ie = varName.indexOf("md") === 0 ? (varName.substr(2, 1).toLowerCase() + varName.substr(3)) : varName;
			_self[ie] = EBG.API.Ad.getCustomVar(uid, varName);
		},

		//this one lets you append strings to an existing string custom var, with optional delimiter
		setDefaultWithAppend: function(varName, defaultValue, optionalDelimiter)
		{
			var delim = optionalDelimiter || "";
			var val = EBG.API.Ad.getCustomVar(uid,varName);		//see if we already have a string in there
			//_self.log("setDefaultWithAppend, current = "+val);
			val = typeof val == "string" ? (val + delim + defaultValue) : defaultValue;
			//_self.log("setDefaultWithAppend, new = "+val);
			_self.setDefault(varName, val, true);
		},
		//-------------------------------------------------
		//End of Custom Vars Related Functions Section
		//=================================================	

		//=================================================
		// Utility Functions
		//-------------------------------------------------
		log: function()
		{   // this is a closure-compiled version of the original code
			if(_self.isDebug){var b,a,c;b=Array.prototype.slice.call(arguments);a=new Date;a=scriptName+" ("+a.getFullYear()+"-"+a.getMonth()+"-"+a.getDate()+" "+a.getHours()+":"+a.getMinutes()+":"+a.getSeconds()+"."+a.getMilliseconds()+"): ";b.unshift(a);try{window.console&&console.logconsole.log&&(console.log.apply?console.log.apply(console,b):console.log(b))}catch(d){(function(a){c=function(a){throw Error("DEBUG: "+a);};setTimeout(c(a),1)})(b.join())}}
		},
		
		removeAd: function()
		{
			EBG.API.Adaptor.removeElement(_self.ebDiv.id);
		},
		
		trim: function(str){
			return (str.replace(/^\s*/,"")).replace(/\s*$/,"");
		},

		addWindowListener: function(eventName, handlerIndex, func)	//add an overrideable listener
		{
			_self.handlers[handlerIndex] = function(){return func.apply(this,arguments);};	//make this handler overrideable by plugin script
			if(_self.displayWin.addEventListener)
				_self.displayWin.addEventListener(eventName, _self.handlers[handlerIndex], false);
			else if(_self.displayWin.attachEvent)
				_self.displayWin.attachEvent(on+eventName, _self.handlers[handlerIndex]);
		},

		removeWindowListener: function(eventName, handlerIndex, func)	//remove an overrideable listener
		{
			if(_self.displayWin.removeEventListener)
				_self.displayWin.removeEventListener(eventName, _self.handlers[handlerIndex], false);
			else if(_self.displayWin.detachEvent)
				_self.displayWin.detachEvent(on+eventName, _self.handlers[handlerIndex]);
			delete _self.handlers[handlerIndex];
		},
		
		getBooleanOption: function(options)
		{
			var opt = options.split("");
			return {
					desktop:		Boolean(opt[0]*1),
					ios:  			Boolean(opt[1]*1),
					android:  		Boolean(opt[2]*1),
					windowsphone:  	Boolean(opt[3]*1)
				};
		},
		
		getBarLeft: function()
		{
			if (_self.isClassicMode)
			{
				return _self.getPanelLeft();
			}
			return 0;
		},
		
		getPanelLeft: function()
		{
			if (_self.panelWidth !== null && !isNaN(_self.panelWidth))
			{
				return Math.floor((_self.getViewPortWidth() - _self.panelWidth) / 2);
			}
			return 0;
		},
		
		getPanelWidth: function()
		{
			if (_self.panelWidth !== null && !isNaN(_self.panelWidth))
			{
				return _self.panelWidth;
			}
			return _self.getViewPortWidth();
		},

		getPanelHeight: function()
		{
			return (_self.getViewPortHeight() - (_self.topMargin + _self.bottomMargin) + _self.androidSnapThinLineFix); // plus 1 to fix the 1px blank underneath the ad
		},

		getTopBarTop: function()
		{
			return (_self.isClassicMode ? EBG.API.Adaptor.getPositioningById(_self.ebDiv.id).Y : 0) + _self.topBarOffsetY;
		},
		
		getBottomBarTop: function()
		{
			return (((_self.isClassicMode ? EBG.API.Adaptor.getPositioningById(_self.ebDiv.id).Y : 0) + _self.getPanelHeight()) - _self.bottomBarHeight) + _self.bottomBarOffsetY;
		},

		getClipTop: function()
		{
			return _self.ebDiv.getBoundingClientRect().top - _self.topMargin;
		},

		getClipBottom: function()
		{
			return _self.ebDiv.getBoundingClientRect().bottom - _self.topMargin;
		},
		
		getViewPortHeight: function()
		{
			return EBG.API.Adaptor.getViewPortMetrics().Height;
		},

		getViewPortWidth: function()
		{
			return EBG.API.Adaptor.getViewPortMetrics().Width;
		},

		getScrollTop: function()
		{
			return (_self.displayWin.pageYOffset !== undefined) ? _self.displayWin.pageYOffset : (_self.displayWinDoc.documentElement || _self.displayWinDocBody.parentNode || _self.displayWinDocBody).scrollTop;
		},

		handleSetCreativeVersion: function(event)	//handle the setCreativeVersion event received from the HTML5 Banner
		{
			_self.versions["creativeIds"] +=	((_self.versions["creativeIds"] !== "" ? "|" : "") + event.creativeId);
			_self.versions["creativeVers"] += ((_self.versions["creativeVers"] !== "" ? "|" : "") + event.creativeVersion);
			_self.versions["creativeLastMods"] += ((_self.versions["creativeLastMods"] !== "" ? "|" : "") + event.creativeLastModified);
		},

		reportCFVersions: function()	//report 'our' versions in this ad, may or may not be called by our own reportCFV function
		{
			_self.isDebug = true;
			var delim = "", s = "reportCFVersions:uid:" +uid+ ": ";

			for(var v in _self.versions){if(_self.versions.hasOwnProperty(v)){s+=(delim+v+": "+_self.versions[v]);delim=", ";}}
			_self.log(s);
		},

		versions:
		{
			"scriptVer":		scriptVersion,
			"scriptLastMod":	lastModified,
			"templateVer":		templateVersion,
			"creativeIds":		"",
			"creativeVers":		"",
			"creativeLastMods":	""
		}
		//-------------------------------------------------
		//End of Utility Functions
		//=================================================
	};
	
	EBG.reportCFV = function(){for(var i in EBG.customFormats)for(var x in EBG.customFormats[i])try{EBG.customFormats[i][x].reportCFVersions();}catch(e){};};
			
    /***************************************************************************/
    /*Initialization : Must be down here after the prototype is fully defined  */
    /***************************************************************************/
	EBG.customFormats[uid][scriptName] = new CustomFormat(); //create our '_self' class object which holds all of our functionality
});