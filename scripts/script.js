/*******************
VARIABLES
*******************/
var adContainer;
var clickThroughButton;
var positionInitialized;
var deviceOrientation;
var sizeCheckerTimer;
var adId;
var rnd;
var uid;
var jewel1, jewel2, jewel3, jewel4, jewel5, arrowLeft, arrowRight;
var swipecontainer;
var carousel;
var currentFrame;
var counter;
var interacted;
var autoloop;
var delay;

/*******************
INITIALIZATION
*******************/
function checkIfAdKitReady(event) {
    adkit.onReady(initializeCreative);
}


window.addEventListener('devicemotion', function(eventData) { 
      //alert(eventData.acceleration.x); 
      //console.log(eventData.acceleration.y); 
      //console.log(eventData.acceleration.z); 
      //console.log(eventData.accelerationIncludingGravity.x); 
      //console.log(eventData.accelerationIncludingGravity.y); 
      //console.log(eventData.accelerationIncludingGravity.z); 
      //console.log(eventData.rotationRate.alpha); 
      //console.log(eventData.rotationRate.beta); 
      //console.log(eventData.rotationRate.gamma); 
}, false); 

function initializeCreative() {
    initializeGlobalVariables();
    addEventListeners();
}

function initializeGlobalVariables() {
    clickThroughButton = document.getElementById("ctalast");
   

    adId = EB._adConfig.adId;
    rnd = EB._adConfig.rnd;
    uid = EB._adConfig.uid;

   
    positionAdElements(true);
}

function initializeVideo() {}

function addEventListeners() {

    // creative events
    clickThroughButton.addEventListener("click", handleClickThrough, false);

    
    

    // format events, please do not remove or alter
    window.addEventListener("resize", handleResize, false);
    window.addEventListener("message", onMessageReceived, false);
    if (EB.API.os.mobile) {
        window.addEventListener(window.navigator.msPointerEnabled ? "MSPointerDown" : "touchstart", handleTouchStart, false);
        window.addEventListener(window.navigator.msPointerEnabled ? "MSPointerUp" : "touchend", handleTouchEnd, false);
    } else {
        window.addEventListener("mouseup", handleTouchEnd, false);
        document.addEventListener(EB.API.browser.firefox ? "DOMMouseScroll" : "mousewheel", handleWheelScroll, false);
        document.addEventListener("keydown", handleKeyDown, false);
    }
}

/*******************
EVENT HANDLERS
*******************/
function handleClickThrough() {
    EB.clickthrough(); // default click through
}

function videoPlaying() {}

function handleResize() {
    positionAdElements();
}

function handleFullScreenChange() {
    // format handler, please do not remove or alter
    if (document.webkitIsFullScreen) {
        EB._sendMessage("creativeEnterFullScreen", {});
    } else {
        EB._sendMessage("creativeExitFullScreen", {});
    }
}

function handleKeyDown() {
    handleWheelScroll();
}

function handleWheelScroll() {
    // format handler, please do not remove or alter
    EB._sendMessage("creativeWheelScroll", {});
}

function handleTouchStart(e) {
    // format handler, please do not remove or alter
    if (typeof e !== "undefined") {
        EB._sendMessage("creativeTouchStart", getTouchPosition(e));
    }
}

function handleTouchEnd(e) {
    // format handler, please do not remove or alter
    if (typeof e !== "undefined") {
        EB._sendMessage("creativeTouchEnd", getTouchPosition(e));
    }
}

/*******************
CREATIVE FUNCTIONS
*******************/
function stopVideo(rewind) {

}

function adScrollIn() {
    EB.userActionCounter("Ad_Scrolled_InToView");
}

function adScrollAway() {
    EB.userActionCounter("Ad_Scrolled_OutOfView");
}

function adSnapIn() {

}

function adSnapOut() {

}

function adCollapsed() {

}

function positionAdElements() {
    EB.API.setStyle(document.body, {
        width: EBG.px(window.innerWidth),
        height: EBG.px(window.innerHeight)
    });

   
    /*	if (EB.API.os.mobile)
    	{
    		if (positionInitialized && !isRotated)
    		{
    			EB.API.setStyle(videoContainer, {
    				
    			});
    		}
    		else
    		{
    			EB.API.setStyle(videoContainer, {
    				
    			});
    		}
    	}*/
    if (!positionInitialized) {
        EB._sendMessage("panelIsLoaded", {});
    }

    if (EB.API.os.android) {
        if (sizeCheckerTimer) {
            clearTimeout(sizeCheckerTimer);
        }
        sizeCheckerTimer = setTimeout(function() {
            if (parseInt(document.body.style.width) !== window.innerWidth || parseInt(document.body.style.height) !== window.innerHeight) {
                positionAdElements();
            }
        }, 500);
    }
    setTimeout(function() {
        positionInitialized = true;
    }, 1000);
}


/*******************
UTILITIES
*******************/
function getTouchPosition(e) {
    if (window.navigator.msPointerEnabled) {
        return { x: e.pageX || event.targetTouches[0].pageX, y: e.pageY || event.targetTouches[0].pageY };
    } else {
        return { x: e.changedTouches[0].pageX, y: e.changedTouches[0].pageY };
    }
}

function getDeviceOrientation() {
}

function getTopBarHeight() {
    return 0;
}

function getBottomBarHeight() {
    return 0;
}

function getStyle(obj, styleName) {
    try {
        if (typeof document.defaultView !== "undefined" && typeof document.defaultView.getComputedStyle !== "undefined") {
            return document.defaultView.getComputedStyle(obj, "")[styleName];
        } else if (typeof obj.currentStyle !== "undefined") {
            return obj.currentStyle[styleName];
        } else {
            return obj.style[styleName];
        }
        return null;
    } catch (error) {
        return null;
    }
}

function onMessageReceived(event) {
    try {
        var data = JSON.parse(event.data);
        if (typeof window[data.type] === "function") {
            window[data.type](data.data);
        }
    } catch (error) {}
}

/*********************************
HTML5 Event System - Do Not Modify
*********************************/
var listenerQueue;
var creativeIFrameId;

function addCustomScriptEventListener(eventName, callback, interAd) {
    listenerQueue = listenerQueue || {};
    var data = {
        uid: uid,
        listenerId: Math.ceil(Math.random() * 1000000000),
        eventName: eventName,
        interAd: !!(interAd),
        creativeIFrameId: creativeIFrameId
    };
    EB._sendMessage("addCustomScriptEventListener", data);
    data.callback = callback;
    listenerQueue[data.listenerId] = data;
    return data.listenerId;
}

function dispatchCustomScriptEvent(eventName, params) {
    params = params || {};
    params.uid = uid;
    params.eventName = eventName;
    params.creativeIFrameId = creativeIFrameId;
    EB._sendMessage("dispatchCustomScriptEvent", params);
}

function removeCustomScriptEventListener(listenerId) {
    var params = {
        uid: uid,
        listenerId: listenerId,
        creativeIFrameId: creativeIFrameId
    };

    EB._sendMessage("removeCustomScriptEventListener", params);
    if (listenerQueue[listenerId])
        delete listenerQueue[listenerId];
}

function eventManager(event) {
    var msg = JSON.parse(event.data);
    if (msg.type && msg.data && (!uid || (msg.data.uid && msg.data.uid == uid))) {
       // alert(msg.type);
        switch (msg.type) {
            case "sendCreativeId":
                creativeIFrameId = msg.data.creativeIFrameId;
                if (creativeContainerReady)
                    creativeContainerReady();
                break;
            case "eventCallback": // Handle Callback
                var list = msg.data.listenerIds;
                var length = list.length;
                for (var i = 0; i < length; i++) {
                    try {
                        var t = listenerQueue[list[i]];
                        if (!t) continue;
                        t.callback(msg.data);
                    } catch (e) {}
                }
                break;
        }
    }
}

window.addEventListener("message", function() {
    try { eventManager.apply(this, arguments); } catch (e) {}
}, false);
/*************************************
End HTML5 Event System - Do Not Modify
*************************************/









window.addEventListener("DOMContentLoaded", checkIfAdKitReady);
